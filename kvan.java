public class kvan {
    public static int depth (Tree x, int i) {
        if (x == null)
            return i;
        else {
            if (x.op == Tree.Op.VAR) {
                if ((x.kvanter != null))
                    i++;
            }
            int i1 = depth(x.left, i);
            int i2 = depth(x.right, i);
            return Math.max(i1, i2);
        }
    }
    public static void main(String[] args) {
        Tree t = new Tree(Tree.Kv.FA, 1, new Tree(Tree.Op.IMP, new Tree(Tree.Op.NOT, new Tree(Tree.Op.AND, new Tree(1), new Tree(Tree.Kv.EX, 0, new Tree(Tree.Op.NOT, new Tree(0))))), new Tree(Tree.Op.OR, new Tree(Tree.Op.NOT, new Tree(2)), new Tree(2))));
        System.out.println(t.toString());
        System.out.print(depth(t, 0));
    }
}