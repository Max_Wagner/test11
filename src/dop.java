import java.util.Arrays;
import java.util.Scanner;

public class dop {
    public static int [] to_2 (int i) {
        int [] l = new int[8];
        Arrays.fill(l, 0);
        int t = 7;
        while ((i != 0) || (t >= 0)){
            l[t] = i%2;
            i = i/2;
            t--;
        }
        return l;
    }
    public static String dp (int [] l) {
        String s = "";
        for (int i = 0; i < l.length; i++) {
            s = s + String.valueOf(1 - l[i]);
        }
        return s;
    }
    public static String norm (int [] l) {
        String s = "";
        for (int i = 0; i < l.length; i++) {
            s = s + String.valueOf(l[i]);
        }
        return s;
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        if (n >= 0) {
            System.out.println(norm(to_2(n)));
        }
        else {
            System.out.println(dp(to_2(Math.abs(n) - 1)));
        }
    }
}