import java.util.Scanner;

public class mas_mult {
    public static String z (int n) {
        String z = "";
        for (int i = 0; i < n; i++) {
            z = z + "0";
        }
        return z;
    }
    public static void mult (String ll, String l1, String l2, int t) {
        if (!l2.equals("")) {
            String l = z(t);
            if (l2.charAt(l2.length() - 1) == '0')
                mult(ll, l1, l2.substring(0, l2.length() - 1), t + 1);
            else {
                l = l1 + l;
                int c = 0;
                char[] lc = l.toCharArray();
                char[] llc = ll.toCharArray();
                for (int i = lc.length; i > lc.length - llc.length; i--) {
                    if (lc[i] != llc[i]) {
                        if (c == 0)
                            lc[i] = '1';
                        else
                            lc[i] = '0';
                    } else {
                        lc[i] = '1';
                        if (lc[i] == '0') {
                            c = '0';
                        }
                    }
                }
                l = "";
                for (int i = 0; i < lc.length; i++) {
                    l = l + lc[i];
                }
                mult(l, l1, l2.substring(0, l2.length() - 1), t + 1);
            }
        }
        else {
            System.out.println(ll);
        }
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String l1 = s.next();
        String l2 = s.next();
        int t1 = l1.length();
        int t2 = l2.length();
        if (t1 >= t2) {
            mult("", l1, l2, 0);
        }
        else {
            mult("", l2, l1, 0);
        }
    }
}