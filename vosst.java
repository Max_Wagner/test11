import java.util.Scanner;

public class vosst {
    public static int n;
    public static int st (int i, int j){
        int x = 1;
        while (j > 0) {
            x = x * i;
            j--;
        }
        return x;
    }
    public static Tree p1 (boolean[] l){
        if (l[n]) {
            Tree t;
            if (l[0]) {
                t = new Tree(0);
            }
            else {
                t = new Tree(Tree.Op.NOT, new Tree(0));
            }
            for (int i = 1; i < n; i++) {
                if (l[i]) {
                    t = new Tree(Tree.Op.AND, t, new Tree(i));
                }
                else {
                    t = new Tree(Tree.Op.AND, t, new Tree(Tree.Op.NOT, new Tree(i)));
                }
            }
            return t;
        }
        else {
            Tree t;
            if (l[0]) {
                t = new Tree(0);
            }
            else {
                t = new Tree(Tree.Op.NOT, new Tree(0));
            }
            for (int i = 1; i < n; i++) {
                if (l[i]) {
                    t = new Tree(Tree.Op.AND, t, new Tree(i));
                }
                else {
                    t = new Tree(Tree.Op.AND, t, new Tree(Tree.Op.NOT, new Tree(i)));
                }
            }
            return new Tree(Tree.Op.NOT, t);
        }
    }
    public static Tree postr (int ch, boolean[][] b){
        Tree t = p1 (b[0]);
        if (ch == 1) {
            return t;
        }
        else {
            for (int j = 1 ; j < n; j++) {
                t = new Tree(Tree.Op.OR, t, p1(b[j]));
            }
            return t;
        }
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("input amount of variables:");
        n = s.nextInt();
        s.nextLine();
        System.out.println("input n + 1 letters (T or F) without spaces or *, if you have inputed all you wanted");
        String p = s.nextLine();
        boolean [] [] b = new boolean[st(2, n)][n+1];
        int ch = 0;
        while (!p.equals("*")) {
            for (int i = 0; i < n + 1; i++) {
                if (p.charAt(i) == 'T') {
                    b[ch][i] = true;
                }
                else {
                    b[ch][i] = false;
                }
            }
            ch++;
            if (ch == st(2, n)) {
                break;
            }
            p = s.nextLine();
        }
        System.out.println((postr (ch, b)).toString());
    }
}
