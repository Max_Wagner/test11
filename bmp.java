import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class bmp {
    public static void main(String[] args)throws IOException {
        File f = new File("In.bmp");
        FileInputStream r = new FileInputStream(f);
        FileOutputStream wr = new FileOutputStream("Out.bmp");
        byte[] ar = new byte[(int)f.length()];
        r.read(ar);
        byte[] pix = {ar[28], ar[29]};
        byte[] hor = {ar[18], ar[19], ar[20], ar[21]};
        byte[] ver = {ar[22], ar[23], ar[24], ar[25]};

        ByteBuffer buf1 = ByteBuffer.wrap(pix);
        buf1.order(ByteOrder.LITTLE_ENDIAN);
        ByteBuffer buf2 = ByteBuffer.wrap(hor);
        buf2.order(ByteOrder.LITTLE_ENDIAN);
        ByteBuffer buf3 = ByteBuffer.wrap(ver);
        buf3.order(ByteOrder.LITTLE_ENDIAN);

        int deep = buf1.getShort();
        deep = deep/8;
        int hori = buf2.getInt();
        int vert = buf3.getInt();

        for(int i=0; i<54; i++){
            wr.write(ar[i]);
        }

        int i = hori*vert*deep + 54;
        while (i >= 54 + hori){
            for(int j = i - hori*deep; j < i; j++){
                wr.write(ar[j]);
            }
            i = i - hori*deep;
        }
        wr.close();
    }
}
