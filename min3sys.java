import java.util.Arrays;
import java.util.Scanner;

public class min3sys {
    static public String [] st;
    static public void addd (Integer i, int sost){
        int n = st.length;
        int sst = (n - i + 1)%2;
        if (i < n) {
            if (sst == 0) {
                addd(i + 1, sost);
                st[i] = "1";
                addd(i + 1, 1);
                st[i] = "2";
                addd(i + 1, 1);
                st[i] = "0";
            }
            else {
                if (sost == 0) {
                    addd(i + 1, 0);
                }
                else {
                    st[i] = "2";
                    addd(i + 1, sost);
                    st[i] = "1";
                    addd(i + 1, 1);
                    st[i] = "0";
                    addd(i + 1, 1);
                }
            }
        }
        else {
            for (String aSt : st) {
                System.out.print(aSt);
            }
            System.out.print("\n");
        }
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        st = new String [n];
        Arrays.fill(st, "0");
        addd(0, 0);
    }
}