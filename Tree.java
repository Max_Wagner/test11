import static java.lang.Character.*;

public class Tree {
    public enum Op{
        AND, OR, NOT, VAR, IMP, EX, FA;
        public String toString() {
            switch (this) {
                case AND: return "&";
                case OR: return "v";
                case NOT: return String.valueOf(toChars (172));
                case VAR: return "VAR";
                case IMP: return "->";
                default: return "???";
            }
        }
    }
    public enum Kv{
        FA, EX, VAR;
        public String toString() {
            switch (this) {
                case FA: return "FA";
                case EX: return "EX";
                case VAR: return "VAR";
                default: return "???";
            }
        }
    }

    public Tree left;
    public Tree right;
    public Kv kvanter;
    public int id;
    public Op op;

    public Tree (Op o, Tree x) {left = x; right = null; kvanter = null; id = 0; op = o;}
    public Tree(Op o,Tree l, Tree r){left = l; right = r; kvanter = null; id = 0; op = o;}
    public Tree (Kv k, int i, Tree x) {left = x; right = null; kvanter = k; id = i; op = op.VAR;}
    public Tree (int i){left = right = null; kvanter = Kv.VAR; op = Op.VAR; id = i;}
    public String toString(){
        switch (op){
            case AND: case OR: case IMP:
                return "(" + left.toString() + op.toString() +
                        right.toString() + ")";
            case NOT:
                return op.toString() + left.toString();
            case VAR:
                switch (kvanter){
                    case FA: case EX:
                        return kvanter.toString() + " A" + Integer.toString(id)+ " " + left.toString();
                    case VAR:
                        return "A" + Integer.toString(id);
                }
            default: return "???";
        }
    }
    public int maxind(){
        switch (op){
            case AND: case OR:case IMP:
                return Math.max (left.maxind(), right.maxind());
            case NOT:
                return left.maxind();
            case EX:case FA:
                return Math.max (left.maxind(), id);
            case VAR:
                return id;
            default: return -1;
        }
    }
    public boolean tf (boolean [] b){
        switch (op){
            case AND:
                return (left.tf(b)) && (right.tf(b));
            case OR:
                return (left.tf(b)) || (right.tf(b));
            case IMP:
                return (((left.tf(b))&&(right.tf(b)))||(!left.tf(b)));
            case NOT:
                return !left.tf(b);
            case FA:
                b[id] = true;
                boolean t = right.tf(b);
                b[id] = false;
                return t && right.tf(b);
            case EX:
                b[id] = true;
                boolean k = right.tf(b);
                b[id] = false;
                return k || right.tf(b);
            case VAR:
                return b[id];
            default: return false;
        }
    }
}