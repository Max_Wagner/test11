import java.util.Arrays;
import java.util.Scanner;

public class tro {
    public static int st (int x, int n) {
        int o = 1;
        while (n > 0) {
            o = o * x;
            n--;
        }
        return o;
    }
    static public int sum (int n) {
        if (n == 0)
            return 1;
        else {
            int x = 1;
            for (int i = n; i > 0; i--) {
                x = 3 * (1 + x);
            }
            return x;
        }
    }
    static public void tr (Integer n){
        if (n == 0)
            System.out.print("0");
        else {
            int i = 1;
            while (!((st(3, i) - sum(i - 1) <= n) && (st(3, i) + sum(i - 1) >= n)))
                i++;
            System.out.print("1");
            n = n - st(3, i);
            for (int j = i - 1; j > 0; j--) {
                if (n >= st(3, j) - sum(j - 1)) {
                    System.out.print("1");
                    n = n - st(3, j);
                }
                else
                    if (n >= - sum(j - 1)) {
                        System.out.print("0");
                    }
                    else {
                        System.out.print("$");
                        n = n + st(3, j);
                    }
            }
            if (n < 0)
                System.out.println("$");
            else
                System.out.println(n);
        }
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        tr (n);
    }
}