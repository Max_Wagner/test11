import java.util.Arrays;

public class Graph
{
    private int [][] myAdj;
    public Graph (int n)
    {
        myAdj = new int [n][n];
        for (int [] i : myAdj)
            Arrays.fill(i, 0);
    }
    public void addEdge (int b , int e) {myAdj [b] [e] = 1;}
    public boolean checkEdge (int b, int e) {return myAdj [b][e] == 1;}
    public void delEdge (int b, int e) {myAdj [b] [e] = 0;}
    public int length () {return myAdj[0].length;};

    //* it's about existence of cycles
    static int [] posesh;
    static int [] m;
    static int k;
    static boolean exists (int [] l, int x){
        int i = 0;
        int t = l.length;
        while (l[i] != x) {
            i++;
            if (i == t)
                break;
        }
        return (i < t);
    }
    boolean if_ac (){
        boolean flag = true;
        posesh [m [k]] = 1;
        for (int i = 0; i < this.length(); i++) {
            if (this.checkEdge(m[k], i)) {
                if ((exists(m, i))&&(k > 0)) {
                    if (m [k - 1] != i) {
                        flag = false;
                        break;
                    }
                }
                else {
                    k++;
                    m[k] = i;
                    flag = flag && if_ac();
                    m[k] = -1;
                    k--;
                }
            }
        }
        return flag;
    }
    public boolean if_acyclic () {
        int n = this.length();
        posesh = new int[n];
        m = new int[n];
        Arrays.fill(posesh, 0);
        boolean fl = true;
        for (int i = 0; i < n; i++) {
            if (posesh[i] != 1) {
                Arrays.fill(m, -1);
                m[0] = i;
                k = 0;
                fl = fl && if_ac();
                if (!fl)
                    break;
            }
        }
        return fl;
    }
}