import java.util.Scanner;

public class zeroone {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int r = s.nextInt();
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < r; j++) {
                if ((i == 0)||(j == 0)||(i == r - 1)||(j == r - 1)) {
                    System.out.print("1");
                }
                else {
                    System.out.print("0");
                }
            }
            System.out.println();
        }
    }

}
