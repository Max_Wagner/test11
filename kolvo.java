import java.util.Scanner;

public class kolvo {
    public static int s;
    public static int n;
    public static int k;
    public static int res (int ost, int pos){
        if (pos == n) {
            if ((ost <= k)&&(ost >= 0)){
                return 1;
            }
            else {
                return 0;
            }
        }
        else {
            int t = 0;
            int u = ost - k * (n - pos);
            if (u <= 0) {
                for (int i = 0; i <= k; i++) {
                    t = t + res(ost - i, pos + 1);
                }
                return t;
            } else {
                for (int i = u; i <= k; i++) {
                    t = t + res(ost - i, pos + 1);
                }
                return t;
            }
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        s = sc.nextInt();
        n = sc.nextInt();
        k = sc.nextInt();
        System.out.println(res(s, 1));
    }
}