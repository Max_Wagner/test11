import java.util.Scanner;

public class to2 {
    public static int st (int x, int n) {
        int o = 1;
        while (n > 0) {
            o = o * x;
            n--;
        }
        return o;
    }
    public static void to_2 (int n) {
        int i = 0;
        while (st(2,i) <= n)
            i++;
        for (int j = i - 1; j >= 0; j--) {
            int t = st(2, j);
            if (n/t == 1)
                System.out.print("1");
            else
                System.out.print("0");
            n = n%t;
        }
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        to_2(n);
    }
}
