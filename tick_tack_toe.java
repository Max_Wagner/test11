import java.util.Scanner;

public class tick_tack_toe {

    public static int n;
    public static int ch;

    public static boolean vctx(char[][] t) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                boolean flag1 = true;
                boolean flag2 = true;
                boolean flag3 = true;
                boolean flag4 = true;
                if (i <= n - ch) {
                    for (int k = 0; k < ch; k++) {
                        flag1 = flag1 && (t[i+k][j] == 'x');
                    }
                    if (flag1) {
                        return true;
                    }
                }
                if (j <= n - ch) {
                    for (int k = 0; k < ch; k++) {
                        flag2 = flag2 && (t[i][j + k] == 'x');
                    }
                    if (flag2) {
                        return true;
                    }
                }
                if ((i <= n - ch) && (j <= n - ch)) {
                    for (int k = 0; k < ch; k++) {
                        flag3 = flag3 && (t[i + k][j + k] == 'x');
                    }
                    if (flag3) {
                        return true;
                    }
                }
                if ((i <= n - ch) && (j >= ch - 1)) {
                    for (int k = 0; k < ch; k++) {
                        flag4 = flag4 && (t[i + k][j - k] == 'x');
                    }
                    if (flag4) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean vcto(char[][] t) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                boolean flag1 = true;
                boolean flag2 = true;
                boolean flag3 = true;
                boolean flag4 = true;
                if (i <= n - ch) {
                    for (int k = 0; k < ch; k++) {
                        flag1 = flag1 && (t[i+k][j] == 'o');
                    }
                    if (flag1) {
                        return true;
                    }
                }
                if (j <= n - ch) {
                    for (int k = 0; k < ch; k++) {
                        flag2 = flag2 && (t[i][j + k] == 'o');
                    }
                    if (flag2) {
                        return true;
                    }
                }
                if ((i <= n - ch) && (j <= n - ch)) {
                    for (int k = 0; k < ch; k++) {
                        flag3 = flag3 && (t[i + k][j + k] == 'o');
                    }
                    if (flag3) {
                        return true;
                    }
                }
                if ((i <= n - ch) && (j >= ch - 1)) {
                    for (int k = 0; k < ch; k++) {
                        flag4 = flag4 && (t[i + k][j - k] == 'o');
                    }
                    if (flag4) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean gamex(char[][] t) {
        char[][] p;
        p = new char[n][n];

        if (vcto(t)) {
            return false;
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (t[i][j] == ' ') {
                    for (int k = 0; k < n; k++) {
                        for (int l = 0; l < n; l++) {
                            if ((k == i) && (l == j)) {
                                p[k][l] = 'x';
                            } else {
                                p[k][l] = t[k][l];
                            }
                        }
                    }
                    if (vctx(p)) {
                        return true;
                    }
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (t[i][j] == ' ') {
                    for (int k = 0; k < n; k++) {
                        for (int l = 0; l < n; l++) {
                            if ((k == i) && (l == j)) {
                                p[k][l] = 'x';
                            } else {
                                p[k][l] = t[k][l];
                            }
                        }
                    }
                    if (gameo(p)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean gameo(char[][] t) {
        char[][] p;
        p = new char[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (t[i][j] == ' ') {
                    for (int k = 0; k < n; k++) {
                        for (int l = 0; l < n; l++) {
                            if ((k == i) && (l == j)) {
                                p[k][l] = 'o';
                            } else {
                                p[k][l] = t[k][l];
                            }
                        }
                    }
                    if (vcto(p)) {
                        return false;
                    }
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (t[i][j] == ' ') {
                    for (int k = 0; k < n; k++) {
                        for (int l = 0; l < n; l++) {
                            if ((k == i) && (l == j)) {
                                p[k][l] = 'o';
                            } else {
                                p[k][l] = t[k][l];
                            }
                        }
                    }
                    if (!gamex(p)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("input n & ch and then press Enter key");
        n = s.nextInt();
        ch = s.nextInt();
        s.nextLine();
        System.out.println("input field n*n");
        char [] [] x = new char[n] [n];
        for (int i = 0; i < n; i++) {
            x[i] = s.nextLine().toCharArray();
        }
        if (gamex(x)){
            System.out.println("true");
        }
        else {
            System.out.println("false");
        }
    }
}
