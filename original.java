import java.util.Arrays;
import java.util.Scanner;

public class original {

    public static class result {
        public enum value {
            Win, Lose, Draw;
        }
        public value val;
        public String toString() {
            switch (val) {
                case Win:return "Win";
                case Lose:return "Lose";
                case Draw:return "Draw";
                default:return "???";
            }
        }
        public boolean toBoolean() {
            switch (val) {
                case Win: return true;
                case Lose:case Draw: return false;
                default:return false;
            }
        }
    }

    public static boolean vctx(char[][] t) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                boolean flag1 = true;
                boolean flag2 = true;
                boolean flag3 = true;
                boolean flag4 = true;
                if (i == 0) {
                    for (int k = 0; k < 3; k++) {
                        flag1 = flag1 && (t[k][j] == 'x');
                    }
                    if (flag1) {
                        return true;
                    }
                }
                if (j == 0) {
                    for (int k = 0; k < 3; k++) {
                        flag2 = flag2 && (t[i][k] == 'x');
                    }
                    if (flag2) {
                        return true;
                    }
                }
                if ((i == 0) && (j == 0)) {
                    for (int k = 0; k < 3; k++) {
                        flag3 = flag3 && (t[k][k] == 'x');
                    }
                    if (flag3) {
                        return true;
                    }
                }
                if ((i == 0) && (j == 2)) {
                    for (int k = 0; k < 3; k++) {
                        flag4 = flag4 && (t[k][2 - k] == 'x');
                    }
                    if (flag4) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean vcto(char[][] t) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                boolean flag1 = true;
                boolean flag2 = true;
                boolean flag3 = true;
                boolean flag4 = true;
                if (i == 0) {
                    for (int k = 0; k < 3; k++) {
                        flag1 = flag1 && (t[k][j] == 'o');
                    }
                    if (flag1) {
                        return true;
                    }
                }
                if (j == 0) {
                    for (int k = 0; k < 3; k++) {
                        flag2 = flag2 && (t[i][k] == 'o');
                    }
                    if (flag2) {
                        return true;
                    }
                }
                if ((i == 0) && (j == 0)) {
                    for (int k = 0; k < 3; k++) {
                        flag3 = flag3 && (t[k][k] == 'o');
                    }
                    if (flag3) {
                        return true;
                    }
                }
                if ((i == 0) && (j == 2)) {
                    for (int k = 0; k < 3; k++) {
                        flag4 = flag4 && (t[k][2 - k] == 'o');
                    }
                    if (flag4) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static result gamex(char[][] t) {
        char[][] p;
        p = new char[3][3];
        int sost = 0;
        result r = new result();

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (t[i][j] == ' ') {
                    for (int k = 0; k < 3; k++) {
                        for (int l = 0; l < 3; l++) {
                            if ((k == i) && (l == j)) {
                                p[k][l] = 'x';
                            } else {
                                p[k][l] = t[k][l];
                            }
                        }
                    }
                    if (vctx(p)) {
                        r.val = result.value.Win;
                        return r;
                    }
                }
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (t[i][j] == ' ') {
                    for (int k = 0; k < 3; k++) {
                        for (int l = 0; l < 3; l++) {
                            if ((k == i) && (l == j)) {
                                p[k][l] = 'x';
                            } else {
                                p[k][l] = t[k][l];
                            }
                        }
                    }
                    switch (gameo(p).val) {
                        case Win: r.val = result.value.Win; return r;
                        case Draw: sost = 1;
                        case Lose: default: sost = sost;
                    }
                }
            }
        }
        if (sost == 1) {
            r.val = result.value.Draw;
            return r;
        }
        else {
            r.val = result.value.Lose;
            return r;
        }
    }

    public static result gameo(char[][] t) {
        char[][] p;
        p = new char[3][3];
        result r = new result();
        int sost = 0;
        boolean flag = false;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (t[i][j] == ' ') {
                    flag = true;
                }
            }
        }
        if (!flag) {
            r.val = result.value.Draw;
            return r;
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (t[i][j] == ' ') {
                    for (int k = 0; k < 3; k++) {
                        for (int l = 0; l < 3; l++) {
                            if ((k == i) && (l == j)) {
                                p[k][l] = 'o';
                            } else {
                                p[k][l] = t[k][l];
                            }
                        }
                    }
                    if (vcto(p)) {
                        r.val = result.value.Lose;
                        return r;
                    }
                }
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (t[i][j] == ' ') {
                    for (int k = 0; k < 3; k++) {
                        for (int l = 0; l < 3; l++) {
                            if ((k == i) && (l == j)) {
                                p[k][l] = 'o';
                            } else {
                                p[k][l] = t[k][l];
                            }
                        }
                    }
                    switch (gamex(p).val) {
                        case Lose: r.val = result.value.Lose; return r;
                        case Draw: sost = 1;
                        case Win: default: sost = sost;
                    }
                }
            }
        }
        if (sost == 1) {
            r.val = result.value.Draw;
            return r;
        }
        else {
            r.val = result.value.Win;
            return r;
        }
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        char [] [] x = new char[3] [3];
        result r = new result();
        int ch1 = 0;
        int ch2 = 0;
        for (int i = 0; i < 3; i++) {
            Arrays.fill(x[i], ' ');
        }
        for (int i = 0; i < 3; i++) {
            x[i] = s.nextLine().toCharArray();
            for (int j = 0; j < 3; j++) {
                if (x[i][j] == 'x'){
                    ch1++;
                }
                if (x[i][j] == 'o'){
                    ch2++;
                }
            }
        }
        if (ch1 == ch2) {
            System.out.println("x " + gamex(x).toString());
        }
        else {
            System.out.println("x " + gameo(x).toString());
        }
    }
}
