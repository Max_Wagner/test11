public class sat {
    public static Tree t;
    public static int n;
    public static void f (boolean [] b, int i){
        if (i == n) {
            for (int j = 0; j < n; j++) {
                if (b[j]) {
                    System.out.print("T  ");
                }
                else {
                    System.out.print("F  ");
                }
            }
            if (t.tf(b)) {
                System.out.print("T  ");
            }
            else {
                System.out.print("F  ");
            }
            System.out.println();
        }
        else {
            b[i] = false;
            f (b, i + 1);
            b[i] = true;
            f (b, i + 1);
        }
    }
    public static void main(String[] args) {
        t = new Tree(Tree.Op.IMP, new Tree(Tree.Op.NOT, new Tree(Tree.Op.AND, new Tree(1), new Tree(Tree.Op.NOT, new Tree(0)))), new Tree(Tree.Op.OR, new Tree(Tree.Op.NOT, new Tree(1)), new Tree(2)));
        System.out.println(t.toString());
        n = t.maxind();
        n++;
        boolean [] b = new boolean[n];
        for (int i = 0; i < n; i++) {
            System.out.print("A" + Integer.toString(i) + " ");
        }
        System.out.println();
        f(b, 0);
    }
}
