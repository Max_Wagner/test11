import java.util.*;
import java.lang.*;
import java.io.*;

class Ideone
{
	public static void fst_step (int k, String s) {
		if (k != 0) {
			snd (k - 1, "1");
			snd (k - 1, "2");
		}
	}
	public static void fst (int k, String s) {
		if (k != 0) {
			snd (k - 1, s + "0");
			snd (k - 1, s + "1");
			snd (k - 1, s + "2");
		}
	}
	public static void snd (int k, String s) {
		if (k != 0) {
			fst (k - 1, s + "2");
			fst (k - 1, s + "1");
			fst (k - 1, s + "0");
		}
		else {
			System.out.println (s);
		}
	}
	public static void main (String[] args) throws java.lang.Exception
	{
		Scanner s = new Scanner (System.in);
		System.out.println ("input n (n mod 2 = 1):");
		int n = s.nextInt();
		System.out.println();
		fst_step (n, "");
	}
}