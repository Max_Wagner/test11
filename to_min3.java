import java.util.Scanner;

public class to_min3 {
    public static int sum_min;
    public static int sum_pl;
    public static int st (int x, int n) {
        int o = 1;
        while (n > 0) {
            o = o * x;
            n--;
        }
        return o;
    }
    public static void start (int x) {
        if (x == 0) {
            System.out.print("0");
        }
        else {
            int i = 0;
            sum_min = 0;
            sum_pl = 0;
            while (x > 2 * st(3, i)) {
                sum_min = sum_min + 2 * st(3, i + 1);
                sum_pl = sum_pl + 2 * st(3, i);
                i = i + 2;
            }
            if (x <= st(3, i) + sum_pl) {
                System.out.print("1");
                min(x - st(3, i), i - 1);
            }
            else {
                System.out.print("2");
                min(x - 2 * st(3, i), i - 1);
            }
        }
    }
    public static void min (int x, int i){
        if (i != -1) {
            if (x == 0) {
                System.out.print("0");
                pl(0, i - 1);
            }
            else {
                int t = st(3, i);
                sum_min = sum_min - 2 * t;
                if (x + sum_min >= 0) {
                    System.out.print("0");
                    pl(x, i - 1);
                }
                else {
                    x = x + t;
                    if (x + sum_min >= 0) {
                        System.out.print("1");
                        pl(x, i - 1);
                    }
                    else {
                        System.out.print("2");
                        pl(x + t, i - 1);
                    }
                }
            }
        }
    }
    public static void pl (int x, int i){
        if (x == 0) {
            System.out.print("0");
            min(0, i - 1);
        }
        else {
            if (i == 0) {
                System.out.print(x);
            }
            else {
                int t = st(3, i);
                sum_pl = sum_pl - 2 * t;
                if (x <= sum_pl) {
                    System.out.print("0");
                    min(x, i - 1);
                } else {
                    x = x - t;
                    if (x <= sum_pl) {
                        System.out.print("1");
                        min(x, i - 1);
                    } else {
                        System.out.print("2");
                        min(x - t, i - 1);
                    }
                }
            }
        }
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        start(n);
    }
}